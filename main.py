#!/usr/bin/python3.6
import tensorflow as tf
# import matplotlib.pyplot as plt

mnist = tf.keras.datasets.mnist  # 28x28 images of hand written images from 0-9

(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Making easier for network to learn ( not necessary  needed) 0-1
x_train = tf.keras.utils.normalize(x_train, axis=1)
x_test = tf.keras.utils.normalize(x_test, axis=1)

# Input Layer
model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Flatten())
# Hidden Layer
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
# Second Layer
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
# Output Layer
model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])


model.fit(x_train, y_train, epochs=3)

val_loss, val_acc = model.evaluate(x_test, y_test)
print(val_loss, val_acc)

# plt.imshow(x_train[0], cmap=plt.cm.binary)  # first img from dataset in  black n white
# plt.show()
